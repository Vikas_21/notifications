<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('delete:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('logs', 'LogsController@view');
Route::get('log', 'LogsController@vw');
Route::post('login', 'LogsController@login');
Route::post('logout', 'LogsController@logout');