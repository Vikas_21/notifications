<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/login', function () {
    return view('auth/login');
});

Route::get('/home/delete', function () {
    return view('auth.delete');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/event', 'EventsController@create')->name('event');

Route::get('/home/viewEvents', 'EventsController@viewEvents')->name('viewEvents');

Route::get('/home/events/{id}', 'EventsController@drop')->name('drop');

Route::get('/home/communication/{id}', 'CommunicationsController@show')->name('show');

Route::get('/home/communicaton/{id}', 'CommunicationsController@create')->name('create');

Route::get('/home/usersinfo/{id}', 'UsersController@info')->name('userinfo');

Route::post('/home/createEvent', 'EventsController@store')->name('evt');

Route::get('/home/users', 'UsersController@index')->name('users');

Route::post('/home/users', 'UsersController@sendPush')->name('send-push');

Route::resource('communications', 'CommunicationsController');

Route::post('/dlt', 'EventsController@dlt')->name('dlt')->middleware('delete');

Route::get('/home/com/{id}', 'CommunicationsController@dlt')->name('dlts');

// Route::get('/delt', 'EventsController@dlt');

// Route::get('/communications/create/{value}', 'CommunicationsController@create')->name('creat');

// Route::get('/password/reset', 'HomeController@index')->name('home');

// Route::get('/users', function () {
//     return 'Hello   ';
// });
