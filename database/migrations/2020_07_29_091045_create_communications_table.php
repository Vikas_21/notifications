<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationsTable extends Migration
{
    /**
     * Run the migrations.
    
     * @return void
     */
    public function up()
    {
        Schema::create('communications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('eventId');
            $table->string('occurance');
            $table->string('occuranceUnit');
            $table->string('overrideable');
            $table->string('email');
            $table->string('emailSubject');
            $table->string('emailBody');
            $table->string('firebase');
            $table->string('firebaseSubject');
            $table->string('firebaseBody');
            $table->string('sms');
            $table->string('smsContent');
            $table->string('whatsapp');
            $table->string('whatsappContent');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('eventId')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
