<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model
{
    protected $fillable = [
        'name', 'eventId', 'occurance', 'occuranceUnit', 'overrideable', 'email', 'emailSuject', 'emailBody', 'firebase', 'firebaseSuject', 'firebaseBody', 'sms', 'smsContent', 'whatsapp', 'whatsappContent',
    ];
}
