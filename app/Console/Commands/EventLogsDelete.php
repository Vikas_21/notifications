<?php

namespace App\Console\Commands;

use App\Log;
use App\Event;
use Carbon\Carbon;
use App\Communication;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class EventLogsDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete user logs once all the communication regarding registered event are made.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $event = Event::all();
        foreach($event as $e){

            // $occurance = DB::table('communications')
            //                 ->select('occurance')
            //                 ->where('eventId', $e['id'])
            //                 ->sum('occurance');

            
            $com = DB::table('communications')->where('eventId', $e['id'])->get();
            // $val = $com['id'];
            // echo $val;
            $max=0;
            foreach($com as $c){
                if($max < (int)$c->occurance)
                {
                    $max = (int)$c->occurance;
                }
            }
            // echo $max;
            $time = Carbon::now()->subMinutes($max);
            $logs = DB::table('event_logs')->where([
                        ['eventId', $e['id'] ],
                        ['createdAt', '<', $time],
                        ])->get();
            foreach($logs as $l){
                Log::destroy($l->id);
            }
            // echo "--";
        }
        $this->info('Logs Deleted successfully');
    }
}
