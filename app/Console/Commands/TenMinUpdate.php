<?php

namespace App\Console\Commands;

use App\Log;
use App\User;
use Carbon\Carbon;
use App\Communication;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
// use App\Providers\FirebaseServiceProvider;
use App\Console\Commands\AccountActivated;




class TenMinUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends Notification Every 10 Minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $user = User::all();
        // foreach ($user as $u)
        // {
        //     echo $u;
        // }

        $com = Communication::all();
        foreach($com as $c)
        {   
            $logEvents = DB::table('event_logs')->where('eventId',  $c['eventId'])->get();
            foreach($logEvents as $l)
            {
                // $selectedTime = $u->created_at;
                if( $l->createdAt < Carbon::now()->subMinutes($c->occurance) && $l->createdAt > Carbon::now()->subMinutes(10+$c->occurance))
                {
                    if($c->email == 1){
                        $temp=strlen($c->emailBody);
                        $temp=$temp-7;
                        $mailContent=substr($c->emailBody,3,$temp);
                        Mail::raw($mailContent, function($message) use ($l,$c)
                        {
                            $message->from('admin@ems.com');
                            $message->to($l->userEmail)->subject($c->emailSubject);
                            // DB::insert('insert into notify (email, communication) values (?, ?)', [$u->email, $c->name]);

                        });
                    }

                    if($c->firebase == 1)
                    {
                        echo "Firebase Notification";
                    }
                    
                    
                }
                
            }

        }
        $this->info('10 Minutes Update has been send successfully');
 
    }
}
