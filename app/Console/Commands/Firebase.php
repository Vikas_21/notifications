<?php

require_once '../../../vendor/autoload.php';



use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\ServiceAccount;



$factory = (new Factory)->withServiceAccount(__DIR__.'/secret/notificusinglaravel-7652ccc13458.json');

$notification = [
            'title' => 'Post title',
            'body' => 'This should probably be longer.'
            ];

$data = [
    'title' => 'Post title',
    'body' => 'This should probably be longer.'
];
// --------------- trying to fetch data
// $database = $factory->createDatabase();
// $reference = $database->getReference('1');
// $value = $reference->getValue();
// echo $value;
// -------------------- trying to store data
$message = CloudMessage::withTarget('token', $deviceToken)->withNotification($notification)->withData($data);

$messaging = $factory->createMessaging();
$messaging->send($message);

// -------------------- old version
