<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Support\Facades\DB;
use App\Communication;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {

        return view('home.event.create');
    }


    public function store(Request $request,Event $event)
    {
        $event->name = $request->name;
        $event->description = $request->description;
        $event->save();
        $arr['events'] = DB::table('events')->paginate(10);
        return view('home.event.index')->with($arr);
    }

    public function drop($id)
    {
        $event = Event::find($id);
        $com = Communication::all();
        foreach($com as $c){
            
            if($c->eventId == $event->id)
            {
                // dd($c->eventId == $event->id);
                Communication::destroy($c->id);
            }
        }
        Event::destroy($id);
        $arr['events'] = DB::table('events')->paginate(10);
        return view('home.event.index')->with($arr);
    }


    public function dlt(Request $request)
    {
        echo $request;
        // $arr['users'] = User::find($id);
        // return view('home.users.info')->with($arr);
    }

    public function viewEvents()
    {   
        
        $arr['events'] = DB::table('events')->paginate(10);
        return view('home.event.index')->with($arr);
        
    }
}
