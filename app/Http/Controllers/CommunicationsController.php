<?php

namespace App\Http\Controllers;

use App\Communication;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommunicationsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $arr['com'] = Communication::all();
       
        return view('home.events.index')->with($arr);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // dd($id);
        $arr['event'] = Event::find($id);
        return view('home.events.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Communication $com)
    {
        $com->name = $request->name;
        // $event = new Event;
        $events = DB::table('events')->select('id')->where('name',  $request->event_name)->first();
        // dd($event->id);
        $com->eventId = $events->id;
        // dd($com->eventId);
        $com->occurance = $request->occurance;
        $com->occuranceUnit = $request->occuranceUnit;
        if($com->occuranceUnit=="days"){
            $com->occurance = $com->occurance*24*60;
        }
        elseif($com->occuranceUnit=="hours"){
            $com->occurance = $com->occurance*60;
        }
        $com->overrideable = 0+$request->overrideable;
        $com->email = 0 + $request->email;
        if($com->email == 0){
            $com->emailSubject = null;
            $com->emailBody = null;
        }
        else{
            $com->emailSubject = $request->emailSubject;
            $com->emailBody = $request->emailBody;
        }
        $com->firebase = 0 + $request->firebase;
        if($com->firebase == 0){
            $com->firebaseSubject = null;
            $com->firebaseBody = null;
        }
        else{
            $com->firebaseSubject = $request->firebaseSubject;
            $com->firebaseBody = $request->firebaseBody;
        }
        $com->sms = 0 + $request->sms;
        if($com->sms == 0){
            $com->smsContent = null;        
        }
        else{
            $com->smsContent = $request->smsContent;        
        }
        $com->whatsapp = 0 + $request->whatsapp;
        if($com->whatsapp == 0){
            $com->whatsappContent = null;     
        }
        else{
            $com->whatsappContent = $request->whatsappContent;    
        }// dd($com);    
        $com->save();
        // dd($request);
        // $arr['event'] = Event::find($id);
        // $events = DB::table('events')->select('name')->get();
        $arr['event'] = DB::table('events')->where('name',  $request->event_name)->first();
        // dd($arr);
        return view('home.events.index')->with($arr);
        // return redirect('communications');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $arr['event'] = Event::find($id);
        // DB::table('events')->paginate(10);
        return view('home.events.index')->with($arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $com = Communication::find($id);
        $arr['com'] = $com;
        return view('home.events.edit')->with($arr);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $com = Communication::find($id);
        $com->name = $request->name;
        // $event = new Event;
        $events = DB::table('events')->select('id')->where('name',  $request->eventName)->first();
        // dd($event->id);
        $com->eventId = $events->id;
        // dd($com->eventId);
        $com->occurance = $request->occurance;
        $com->occuranceUnit = $request->occuranceUnit;
        if($com->occuranceUnit=="days"){
            $com->occurance = $com->occurance*24*60;
        }
        elseif($com->occuranceUnit=="hours"){
            $com->occurance = $com->occurance*60;
        }
        $com->overrideable = 0+$request->overrideable;
        $com->email = 0 + $request->email;
        if($com->email == 0){
            $com->emailSubject = null;
            $com->emailBody = null;
        }
        else{
            $com->emailSubject = $request->emailSubject;
            $com->emailBody = $request->emailBody;
        }
        $com->firebase = 0 + $request->firebase;
        if($com->firebase == 0){
            $com->firebaseSubject = null;
            $com->firebaseBody = null;
        }
        else{
            $com->firebaseSubject = $request->firebaseSubject;
            $com->firebaseBody = $request->firebaseBody;
        }
        $com->sms = 0 + $request->sms;
        if($com->sms == 0){
            $com->smsContent = null;        
        }
        else{
            $com->smsContent = $request->smsContent;        
        }
        $com->whatsapp = 0 + $request->whatsapp;
        if($com->whatsapp == 0){
            $com->whatsappContent = null;     
        }
        else{
            $com->whatsappContent = $request->whatsappContent;    
        }
        // dd($com);    
        $com->save();
        // dd($request);
        // $arr['event'] = Event::find($id);
        // $events = DB::table('events')->select('name')->get();
        $arr['event'] = DB::table('events')->where('name',  $request->eventName)->first();
        // dd($arr);
        return view('home.events.index')->with($arr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dlt($id)
    {
        $com = Communication::find($id);
        $arr['event'] = DB::table('events')->where('name',  $com->eventName)->first();
        Communication::destroy($id);
        // dd($arr);
        return view('home.events.index')->with($arr);
    }
}
