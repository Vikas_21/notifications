<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    protected $serverKey;

    public function __construct()
    {
        $this->middleware('auth');
        // $this->serverKey = config('AIzaSyBx8KjgKZjfmfjtN3JCrN6WseyS9fW-CKQ');
    }
    public function index()
    {
        $arr['user'] = DB::table('event_logs')->paginate(10);
        return view('home.users.index')->with($arr);
    }

    public function info($id)
    {
        $arr['users'] = Log::find($id);
        return view('home.users.info')->with($arr);
    }

    public function dlt($email, $password, $event)
    {
        echo $event;
        // $arr['users'] = User::find($id);
        // return view('home.users.info')->with($arr);
    }
    


}