<?php

namespace App\Http\Controllers;

use App\Log;
use App\Event;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function view(){
        return response()->json(Log::get(),200);
    }


    public function vw(){
        return "Hello";
    }


    public function login(Request $request){
        // return response()->json(["message"=>"Error in function"],400);
        $rules = [
            'userId' => 'required',
            'userName' => 'required|min:2',
            'companyName' => 'required|min:2',
            'eventName' => 'required|min:3',
            'phoneNumber' => 'required|min:10|max:13',
            'userEmail' => 'required|min:8',
        ];
        $log = new Log;
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        // return response()->json(["message"=>"Validation passed"],400);
        $log['userId']=$request['userId'];
        // return response()->json(["message"=>"UserId saved"],400);
        $log['userName']=$request['userName'];
        $log['companyName']=$request['companyName'];
        $log['userEmail']=$request['userEmail'];
        $log['phoneNumber']=$request['phoneNumber'];
        if($request['extraInfo']){
            $log['extraInfo']=$request['extraInfo'];
        }
        else{
            $log['extraInfo']=null;
        }
        // return response()->json(["message"=>"Event saved in log variable"],400);
        // return response()->json($request['eventName'],400);
        // return response()->json(DB::table('events')->where('name', $request['eventName'])->first(),400);
        // $event = new Event;
        // $events = DB::table('events')->select('id')->where('name',  $request->event_name)->first();
        $event = DB::table('events')->where('name', $request['eventName'])->first();
        // return response()->json($event->id,400);
        if(is_null($event->name)){
            return response()->json(["message"=>"Event not found"],400);
        }
        $log['eventId']=$event->id;
        // return response()->json(["message"=>"Event Id saved"],400);
        // $flag=0;
        $logEvents = DB::table('event_logs')->where('userId',  $log['userId'])->get();
        // return response()->json($logEvents,400);
        
        
        foreach($logEvents as $l)
        {   
            if($l->eventId == $event->id)
            {
                return response()->json(["message"=>"Event already registered by the user"],400);
            }
        }
        // $event = Event::where('name','=',$request->event);
        // if(is_null($event))
        // {
        //     return response()->json(["message"=>"Invalid Event"],400);
        // }
        // if($flag==0)
        // {
        //     DB::table('counts')->insert(['email'=> $request['email']]);
        // }
        // return response()->json($log,400);
        $log->save();
        return response()->json(["message"=>"Registered Successfully"],201);
        // return response()->json(Log::get(),200);
    }

    public function logout(Request $request){
        $event = DB::table('events')->where('name',  $request['eventName'])->first();
        if(is_null($event->name)){
            return response()->json(["message"=>"Event not found"],400);
        }
        // return response()->json($event,400);
        $users = DB::table('event_logs')->where('eventId',  $event->id)->get();
        // return response()->json($users,400);
        foreach($users as $u){
            if($u->userEmail==$request['userEmail'])
            {
                Log::destroy($u->id);    
                return response()->json(["message"=>"Logged out Successfully"],201); 
            }
        }
        return response()->json(["message"=>"Invalid Credentials"],400);

    }
}
