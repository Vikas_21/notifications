<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Notifiable;



class Log extends Model
{
    protected $table = "event_logs";

    protected $fillable = [
        'userId', 'userName', 'companyName', 'userEmail', 'phoneNumber', 'eventId', 'extraInfo',
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }
}
