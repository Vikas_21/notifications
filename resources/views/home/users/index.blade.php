
@extends('layouts.admin')

@section('content')



<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users Events</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('viewEvents') }}">Events</a></li>
              <li class="breadcrumb-item active"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



<section class="content">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>User Name</th>
                <th>Event</th>
                <th>Action</th>
            </tr>
            @foreach($user as $u)
            <tr>
                <td>{{ $u->userName }}</td>
                <?php 
                $event = DB::table('events')->where('id',  $u->eventId)->first();
                ?>
                <!-- $event->id -->
                <td>{{ $event->name }}</td>
                <td><a href="{{ route('userinfo',$u->id) }}" class="btn btn-info">View User Info</a></td>
            </tr>
            @endforeach
        </table>
        <div class="content">
        {{$user->links()}}
        </div>
        
    </div>
</section>


    




@endsection