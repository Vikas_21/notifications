
@extends('layouts.admin')

@section('content')



<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users Information</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('viewEvents') }}">Events</a></li>
              <li class="breadcrumb-item active"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



<section class="content">
    <div class="container-fluid">
        <form action="">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Users Name</label>
                <div class="col-md-6"><input type="text" class="form-control" required value="{{$users->userName}}" readonly></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Users Company Name</label>
                <div class="col-md-6"><input type="text" class="form-control" required value="{{$users->companyName}}" readonly></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Users Phone Number</label>
                <div class="col-md-6"><input type="text" class="form-control" required value="{{$users->phoneNumber}}" readonly></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Users Email ID</label>
                <div class="col-md-6"><input type="text" class="form-control" required value="{{$users->userEmail}}" readonly></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Registered At</label>
                <div class="col-md-6"><input type="text" class="form-control" required value="{{$users->createdAt}}" readonly></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </form>
    </div>
</section>



@endsection