@extends('layouts.admin')

@section('content')



<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Events</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('communications.index') }}">Events</a></li>
              <li class="breadcrumb-item active"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



<section class="content">
    <div class="container-fluid">
        <table class="table table-striped">
          <?php
          // use App\Event;
          // $events = DB::table('event_logs')->paginate(10);
          ?>
            <tr>
                <th>Event Name</th>
                <th>Description</th>
                <th></th>
                
            </tr>
            @foreach($events as $e)
              <tr>
                <th>{{ $e->name }}</th>
                <th>{{ $e->description }}</th>
                <th><button class="btn bg-transparent" id="btnPopover{{$e->id}}" type="" data-trigger="focus" data-toggle="popover" 
                data-content="<a href='/home/communication/{{$e->id}}'>View</a><br>
                <a href='/home/events/{{$e->id}}'>Delete</a>"
                data-html="true">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i></button></th>

                <!-- <tr><a href="{{ route('show',$e->id)}}">View</a></tr>
                <tr><a href="{{ route('drop',$e->id)}}">{{ $e->name }}</a></tr> -->
                <!-- <th><a href="{{ route('drop',$e->id)}}" class="btn btn-danger">Delete this Event</a></th> -->
                <!-- <th><a href="" onClick="openPopup(this);"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a></th> -->
                <!-- <div id="div1" class="popup" style="display:none;">
                    This is a test message div{{$e->id}}
                    <div class="cancel" onclick="closePopup();"></div>
                </div> -->

              </tr>
            @endforeach
        </table>
        {{$events->links()}}
        <a href="{{ route('event')}}" class="btn btn-primary">Add New Event</a>
    </div>
</section>


@endsection