@extends('layouts.admin')

@section('content')



<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Events</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('communications.index') }}">Events</a></li>
              <li class="breadcrumb-item active"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



<section class="content">
    <div class="container-fluid">
        <table class="table table-striped">
          <?php
          use Illuminate\Support\Facades\DB;
          $com = DB::table('communications')->where('eventId',$event->id)->paginate(10);
          ?>
            <tr>
                <th><a href="{{ route('create',$event->id)}}" class="btn btn-primary">Add Communication</a></th>
                <th></th>
                
            </tr>
              <tr>
                <th>{{ $event->name }} Event</th>
                <th><a href="{{ route('drop',$event->id)}}" class="btn btn-danger">Delete this Event</a></th>
              </tr>
              @foreach($com as $c)
                <tr>
                    <td>{{ $c->name }}</td>
                    <td> 

                    <a href="{{route('communications.edit',$c->id)}}" class="btn btn-info">Edit/ View</a>

                    <a href="{{route('dlts',$c->id)}}" class="btn btn-danger"  >Delete</a>
                    <!-- <a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger"  >Delete</a>
                    <form action="{{route('communications.destroy',$c->id)}}" method="post">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                    </form>

                    </td>
                </tr>

                @endforeach
        </table>
        {{$com->links()}}
    </div>
</section>


@endsection