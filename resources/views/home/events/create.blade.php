@extends('layouts.admin')

@section('content')


<?php
$id=0;
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Communications</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('communications.index')}}">Events</a></li>
              <li class="breadcrumb-item"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<section class="content">
    <div class="container-fluid">
     <form method="post" action="{{route('communications.store')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!-- <div class="form-group">
            <div class="row">
                <label class="col-md-3">Titlt</label>
                <div class="col-md-6"><input type="text" name="title" class="form-control"></div>
                <div class="clearfix"></div>
            </div>
        </div>  -->

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Communication Name</label>
                <div class="col-md-6"><input type="text" name="name" class="form-control" required></div>
                <div class="clearfix"></div>
            </div>
        </div> 

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Event Name</label>
                <div class="col-md-6"><input type="text" name="event_name" class="form-control" value="{{$event->name}}" readonly required></div>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">After What Time Interval</label>
                <div class="col-md-3"><input type="number" name="occurance" class="form-control" placeholder="" required></div>
                <div class="col-md-3">
                    <select id="occuranceUnit" name="occuranceUnit" class="form-control">
                    <option value="minutes">Minutes</option>
                    <option value="hours">Hours</option>
                    <option value="days">Days</option>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>


        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Overrideable</label>
                <input checked class="col-md-1" type="checkbox" id="overrideable" name="overrideable" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="row">
            <label class="col-md-3">Mode of Notification</label>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Email</label>
                <input id="email" checked class="col-md-1" type="checkbox" id="email" name="email" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="emailInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Subject</label>
                <div class="col-md-6"><input type="text" name="emailSubject" class="form-control" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Body</label>
                <div class="col-md-6"><textarea class="form-control" name="emailBody"></textarea></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Firebase Notification</label>
                <input checked class="col-md-1" type="checkbox" id="firebase" name="firebase" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="firebaseInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Subject</label>
                <div class="col-md-6"><input type="text" name="firebaseSubject" class="form-control" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Body</label>
                <div class="col-md-6"><textarea class="form-control" name="firebaseBody"></textarea></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-2">SMS</label>
                <input checked class="col-md-1" type="checkbox" id="sms" name="sms" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="smsInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">SMS Content</label>
                <div class="col-md-6"><input type="text" name="smsContent" class="form-control" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Whats App</label>
                <input checked class="col-md-1" type="checkbox" id="whatsapp" name="whatsapp" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="whatsappInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Whats App Content</label>
                <div class="col-md-6"><input type="text" name="whatsappContent" class="form-control" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-info" value="Save">
        </div>
    </form>
    </div>
</section>

@endsection
