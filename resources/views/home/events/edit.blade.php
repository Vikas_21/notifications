@extends('layouts.admin')

@section('content')



<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit/ View Communications</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="{{ route('home') }}">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="{{ route('communications.index') }}">Events</a></li>
              <li class="breadcrumb-item"><a href="{{ route('users') }}">Users</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
     <form method="post" action="{{route('communications.update',$com->id)}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="put">
        <!-- <div class="form-group">
            <div class="row">
                <label class="col-md-3">Titlt</label>
                <div class="col-md-6"><input type="text" name="title" class="form-control"></div>
                <div class="clearfix"></div>
            </div>
        </div>  -->
<!-- 
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Communication Name</label>
                <div class="col-md-6"><input type="text" name="name" class="form-control" required value="{{$com->name}}"></div>
                <div class="clearfix"></div>
            </div>
        </div>  -->

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Communication Name</label>
                <div class="col-md-6"><input type="text" name="name" class="form-control" required value="{{$com->name}}"></div>
                <div class="clearfix"></div>
            </div>
        </div> 

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Event Name</label>
                <?php
                use App\Event;
                $event = Event::find($com->eventId);
                ?>
                <div class="col-md-6"><input type="text" name="eventName" class="form-control" value="{{$event->name}}" readonly required></div>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">After What Time Interval</label>
                <?php
                if($com->occuranceUnit=="days")
                {
                    $com->occurance = $com->occurance/(24*60);
                }
                elseif($com->occuranceUnit=="hours"){
                    $com->occurance = $com->occurance/60;
                }
                ?>
                <div class="col-md-3"><input type="number" name="occurance" class="form-control" placeholder="" value="{{$com->occurance}}" required></div>
                <div class="col-md-3">
                    <select id="occuranceUnit" name="occuranceUnit" class="form-control">
                    @if ($com->occuranceUnit=="days")
                    <option value="minutes">Minutes</option>
                    <option value="hours">Hours</option>
                    <option value="days" selected>Days</option>
                    @elseif ($com->occuranceUnit=="hours")
                    <option value="minutes">Minutes</option>
                    <option value="hours" selected>Hours</option>
                    <option value="days">Days</option>
                    @else
                    <option value="minutes" selected>Minutes</option>
                    <option value="hours">Hours</option>
                    <option value="days">Days</option>
                    @endif
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Overrideable</label>
                @if ($com->overrideable==1)
                <input checked class="col-md-1" type="checkbox" id="overrideable" name="overrideable" value="1" class="form-control">
                @else
                <input class="col-md-1" type="checkbox" id="overrideable" name="overrideable" value="1" class="form-control">
                @endif
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
            <label class="col-md-3">Mode of Notification</label>
            </div>
        </div>
        @if ($com->email==1)
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Email</label>
                <input id="email" checked class="col-md-1" type="checkbox" id="email" name="email" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="emailInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Subject</label>
                <div class="col-md-6"><input type="text" name="emailSubject" class="form-control"value="{{$com->emailSubject}}" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Body</label>
                <div class="col-md-6">
                <!-- <input type="text" name="emailBody" class="form-control" value="{{$com->emailBody}}" placeholder="" > -->
                <textarea class="form-control" name="emailBody"><?php echo $com->emailBody; ?></textarea>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @else
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Email</label>
                <input id="email" class="col-md-1" type="checkbox" id="email" name="email" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="emailInfo" style="display:none;">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Subject</label>
                <div class="col-md-6"><input type="text" name="emailSubject" class="form-control" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Email Body</label>
                <div class="col-md-6"><textarea class="form-control" name="emailBody"></textarea></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @endif
        
        @if ($com->firebase==1)
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Firebase Notification</label>
                <input checked class="col-md-1" type="checkbox" id="firebase" name="firebase" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="firebaseInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Subject</label>
                <div class="col-md-6"><input type="text" name="firebaseSubject" class="form-control" value="{{$com->firebaseSubject}}" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Body</label>
                <div class="col-md-6">
                <textarea class="form-control" name="firebaseBody"><?php echo $com->firebaseBody; ?></textarea>
                <!-- <input type="text" name="firebaseBody" class="form-control" placeholder="" value="{{$com->firebaseBody}}"> -->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @else
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Firebase Notification</label>
                <input class="col-md-1" type="checkbox" id="firebase" name="firebase" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="firebaseInfo" style="display:none;">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Subject</label>
                <div class="col-md-6"><input type="text" name="firebaseSubject" class="form-control" placeholder="" ></div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Firebase Body</label>
                <div class="col-md-6"><textarea class="form-control" name="firebaseBody"></textarea></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @endif

        @if ($com->sms==1)
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">SMS</label>
                <input checked class="col-md-1" type="checkbox" id="sms" name="sms" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="smsInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">SMS Content</label>
                <div class="col-md-6"><input type="text" name="smsContent" class="form-control" value="{{$com->smsContent}}" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @else
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">SMS</label>
                <input class="col-md-1" type="checkbox" id="sms" name="sms" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="smsInfo" style="display:none;">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">SMS Content</label>
                <div class="col-md-6"><input type="text" name="smsContent" class="form-control" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @endif

        @if ($com->whatsapp==1)
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Whats App</label>
                <input checked class="col-md-1" type="checkbox" id="whatsapp" name="whatsapp" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="whatsappInfo">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Whats App Content</label>
                <div class="col-md-6"><input type="text" name="whatsappContent" class="form-control" value="{{$com->whatsappContent}}" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @else
        <div class="form-group">
            <div class="row">
                <label class="col-md-2">Whats App</label>
                <input class="col-md-1" type="checkbox" id="whatsapp" name="whatsapp" value="1" class="form-control">
                <div class="clearfix"></div>
            </div>
        </div>

        <div id="whatsappInfo" style="display:none;">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3">Whats App Content</label>
                <div class="col-md-6"><input type="text" name="whatsappContent" class="form-control" placeholder=""></div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        @endif

        <div class="form-group">
            <input type="submit" class="btn btn-info" value="Save">
        </div>
    </form>
    </div>
</section>

@endsection
