<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>EMS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }} ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }} ">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }} ">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }} ">
  
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }} ">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }} ">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }} ">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	axisX: {
		// valueFormatString: "string"
	},
	axisY: {
		title: "Count",
		includeZero: true,
		suffix: ""
	},
	legend:{
		cursor: "pointer",
		fontSize: 16,
		itemclick: toggleDataSeries
	},
	toolTip:{
		shared: true
	},
	data: [{
		name: "Total Data Count",
		type: "spline",
		showInLegend: true,
		yValueFormatString: "#,##0# Units",
		dataPoints: [
			{ label: "Events",  y: 
      <?php
      $events = DB::table('events')->select('name')->get();
      echo $events->count(); 
      $v1 = $events->count();
      ?>
      },
			{ label: "Communications", y:
      <?php
      $com = DB::table('communications')->select('name')->get();
      echo $com->count();
        $v2 = $com->count();
      ?>
      },
			{ label: "Users",  y: 
      <?php
     $users = DB::table('counts')->select('email')->get();
     echo $users->count();

      ?>
      },
			{ label: "Notification",  y: 
      <?php
      $sum = 0;
      $events = DB::table('events')->get();
      foreach($events as $e){
        $users = DB::table('event_logs')->where('eventId',$e->id)->get();
        $v1=$users->count();
        $com = DB::table('communications')->where('eventId',$e->id)->get();
        $v2=$com->count();
        $sum=$sum+$v1*$v2;
      }
      // $val=$v1*$v2;
      // echo $val; 
      
      echo $sum;
     

      ?>
      }
    ]
	},
	{
		name: " Current Data Count",
		type: "spline",
		showInLegend: true,
		yValueFormatString: "#,##0# Units",
		dataPoints: [
			{ label: "Events",  y: 
      <?php
      $events = DB::table('events')->select('name')->get();
      echo $events->count(); 
      ?>
      },
			{ label: "Communications", y:
      <?php
      $com = DB::table('communications')->select('name')->get();
      echo $com->count();
      ?>
      },
			{ label: "Users",  y: 
      <?php
      $users = DB::table('event_logs')->distinct()->select('userName')->get();
      echo $users->count();
      ?>
      },
			{ label: "Notification",  y: 
      <?php
      $notify = DB::table('notify')->select('email')->get();
      echo $notify->count();
      ?>
      }
    ]
	},
	]
});
chart.render();

function toggleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else{
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
</script>
  <style>
  div.gallery {
  margin: 5px;
  border: 1px solid #ccc;
  float: justify;
  width: auto;
}
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="home" class="nav-link">Admin Pannel</a>
      </li>
    </ul>

    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    
    <a class="brand-link" href="{{ url('/') }}">
        {{ config('app.name', 'Laravel') }}
    </a>

    <!-- <a href="index3.html" class="brand-link">
      <img src="{{ asset('dist/img/AdminLTELogo.png') }} " alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 ">
        <!-- <div class="image">
          <img src="{{ asset('dist/img/user2-160x160.jpg') }} " class="img-circle elevation-2" alt="User Image">
        </div> -->
        <!-- <div class="info"> -->
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">  
                <a class="nav-link">
                    <i class="nav-icon fas fa-user"></i> 
                    <p>
                        Welcome Admin!
                    </p>
                </a>
            </li>
        </ul>
        </nav>
        <!-- </div> -->
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('viewEvents') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Events
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('users') }}" class="nav-link">
              <i class="nav-icon fas fa-users   "></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-header">Actions</li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                Logout
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; Websites.co.in</strong>
    All rights reserved.
    
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }} "></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }} "></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }} "></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }} "></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }} "></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }} "></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }} "></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }} "></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }} "></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }} "></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }} "></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }} "></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard.js') }} "></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }} "></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script src="{{ asset('ckeditor/ckeditor.js') }} "></script>
<script>
  CKEDITOR.replace('firebaseBody');
  CKEDITOR.replace('emailBody');
</script>
<script>
$(document).ready(function(){
  $("#email").click(function(){
    $("#emailInfo").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
  $("#firebase").click(function(){
    $("#firebaseInfo").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
  $("#sms").click(function(){
    $("#smsInfo").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
  $("#whatsapp").click(function(){
    $("#whatsappInfo").toggle();
  });
});
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php 
$event = DB::table('events')->select('id')->get();
foreach($event as $e){
  ?>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#btnPopover{{$e->id}}').popover();
  });
  </script>  
  <?php
}
?>
<!-- <script type="text/javascript">
$(document).ready(function(){
  $('#btnPopover{{$e->id}}').popover();
});
</script> -->

<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.js') }} "></script>



</body>
</html>
